// TODO: For the css and js bundle add a multi name pattern with an Array

/**
 * @typedef {GulpOptions}
 */
const Options = {
    css: {
        autoprefixer: true,
        cssvariables: true,
        minify: true,
        bundle: {
            // should bundle file?
            compile: true,
            // for every entry will be generated a css file.
            entrys: [
                {
                    // name for this entry (final css page)
                    name: 'main.css',
                    // location of the file
                    // this will already consider the root path for the css folder
                    file: ['./src/css/main.css']
                }
                // ,{
                // name: 'other-file-name.css',
                // file: ['./src/css/other-location.css']
                // }
            ]
        }
    },
    libs: {
        silkUI: true,
        outsystemsUI: true
    },
    js: {
        minify: true,
        bunble: {
            value: true,
            entrys: []
        }
    }
};

module.exports = Options;
