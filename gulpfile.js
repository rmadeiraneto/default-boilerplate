/* eslint-disable require-jsdoc */
// TODO: add cssnano  add source maps
const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const css = require('./config/gulp_css.js');
const sass = require('./config/gulp_sass.js');
const pug = require('./config/gulp_pug.js');
const js = require('./config/gulp_js.js');
const html = require('./config/gulp_html.js');
const docs = require('./config/gulp_docs.js');
const libs = require('./config/gulp_libs.js');

function reload(done) {
    browserSync.reload();
    if (done) done();
}

gulp.task('css', css.compile);
gulp.task('watch:css', css.watch.bind({}, reload));

gulp.task('sass', sass.compile);
gulp.task('watch:sass', sass.watch.bind({}, reload));

gulp.task('libs', libs.compile);

gulp.task('pug', pug.compile);
gulp.task('watch:pug', pug.watch.bind({}, reload));

gulp.task('html', html.compile);
gulp.task('watch:html', html.watch.bind({}, reload));

gulp.task('js', js.compile);
gulp.task('watch:js', js.watch.bind({}, reload));

gulp.task('docs', docs.generate);
gulp.task('bundle', js.bundle);
gulp.task('server', function(done) {
    // Serve files from the root of this project
    browserSync.init({
        server: {
            baseDir: './dist/'
        }
    });

    done();
});

gulp.task(
    'default',
    gulp.series(
        gulp.parallel('css', 'js', 'html', 'pug', 'sass', 'libs'),
        gulp.parallel(
            'server',
            'watch:html',
            'watch:css',
            'watch:js',
            'watch:pug',
            'watch:sass'
        )
    )
);
