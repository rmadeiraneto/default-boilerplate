/**
 * @class
 * @param {BigInteger} height
 * @param {BigInteger} width
 *
 */
class Rectangle {
    /**
     * @constructor
     * @param {*} height
     * @param {*} width
     */
    constructor(height, width) {
        this.height = height;
        this.width = width;
    }
    /**
     * get area for rectangle
     * @method
     */
    get area() {
        return this.calcArea();
    }
    /**
     * calcArea
     * @protected
     * @return {any}
     */
    calcArea() {
        return this.height * this.width;
    }
}

export default Rectangle;
