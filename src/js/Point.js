/**
 *  Class
 */
class Point {
    /**
     *
     * @param {*} x
     * @param {*} y
     */
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
    /**
     *
     * @param {*} a
     * @param {*} b
     * @return {float}
     */
    static distance(a, b) {
        const dx = a.x - b.x;
        const dy = a.y - b.y;
        return Math.hypot(dx, dy);
    }
}

export default Point;
