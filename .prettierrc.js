module.exports = {
    arrowParens: 'avoid',
    bracketSpacing: true,
    htmlWhitespaceSensitivity: 'css',
    useTabs: false,
    tabWidth: 4,
    insertPragma: false,
    jsxBracketSameLine: false,
    jsxSingleQuote: false,
    requirePragma: false,
    semi: true,
    singleQuote: true,
    trailingComma: 'none',
    useTabs: false
};
