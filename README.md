# default-boilerplate

Kickstarter project to create projects with javascript, node.js, css and html files

## Required software

1. [Node.js](https://nodejs.org/) > LTS release
2. [Visual Studio Code](https://code.visualstudio.com/)
    > optional
3. A Git Client like [Sourcetree](https://www.sourcetreeapp.com/) (recommended) , [Kraken](https://www.gitkraken.com/download) or [GitHub Desktop](https://desktop.github.com)
4. Install [Git](https://git-scm.com/downloads) will help too, great bash console.

## How to use this project

> I highly recomend you too create a account on the gitlab.

1. Download this project for your desktop. ( if you have an account fork the project for you personal area )
2. Unzip and change the name for whatever you want.
3. Open Visual Stuido code
4. On VSCode do: File > Open Folder... and select the root folder of the project.
5. On the first time, VSCode should ask you to install recommended extensions. if not press `f1` and in the prompt write `> Show Recommended Extensions`
6. Only for the first time do `control+ç` this will toggle the `terminal`, then on terminal write `npm i` or `npm install`

## Project Configuration

Head to the `configuration.js` file on the root folder, tweak the options on the file to output the files as you need.

![Configuration location](/gitlab/img/configuration.JPG 'Title Text')
![Configuration file](/gitlab/img/configuration_file.JPG 'Title Text')

## Features

### css

    will convert all css files in src/css will all the vendor prefixes needed.

    right now will prefix to the last 2 version of all browsers (IE11, IE10, Firefox, chrome etc...);
    see `browserlist` in package.json if you need for example to give more versions support.

### sass

    Converts sass files to css

### pug

    Converts pug files to under dist folder

### html

    clones and minify html files to dist folder

### js

    parses the code by the babel packages and transforms es6+ to es5

## TODO

1. Separate the compilation of js from js bundle like in css part

2. Svg to FontIcon

3. Svg to Ico

4. Add more detailt to this readme
