const sourcePath = './src';
const destinePath = './dist';
// imported to be possible to remove bundle paths from normal files.
const options = require('../configuration');

/**
 * returns a object with source and dest paths for files.
 * it makes files go from source folder to distribution/build folder
 * @param {string} source source path
 * @param {string} destine destine path
 * @return {ConfigPath} object with src and dest
 */
function mountPathSrcToDest(source, destine) {
    return new ConfigPath(sourcePath + source, destinePath + destine);
}

/**
 * Returns a object with source and dest paths for files.
 * it makes files go from source folder to source folder
 * @param {string} source source path
 * @param {string} destine destine path
 * @return {ConfigPath}
 */
// function mountPathSrcToSrc(source, destine) {
//     return new ConfigPath(sourcePath + source, sourcePath + destine);
// }

/**
 *
 * @param {Array<string>} paths Array with paths to exclude
 * @return {Array<string>} Input Array with all paths negated
 */
function excludePaths(paths) {
    var negatedPaths = [];
    if (paths && paths.length) {
        for (let i = 0; i < paths.length; i++) {
            var element = paths[i];
            if (!element.startsWith('!')) {
                negatedPaths.push('!' + element);
            }
        }
    }
    // returns a string with all elements separted with ','
    return negatedPaths;
}

/**
 * Class that holds the path for the source files and distribution
 * @class
 * @property {string} sPath - origin of the files
 * @property {string} dPath - destination for the files
 */
class ConfigPath {
    /**
     * @param {string|Array<string>} srcPath - Path to the source files
     * @param {string} distPath - Path to the distribution/build path
     */
    constructor(srcPath, distPath) {
        this.sPath = srcPath;
        this.dPath = distPath;
    }
}

const paths = {
    css: new ConfigPath(
        [sourcePath + '/css/**/[^_]*.css'].concat(
            (function() {
                var allPaths = [];
                // joins together all the files in one array of paths
                for (let i = 0; i < options.css.bundle.entrys.length; i++) {
                    var entry = options.css.bundle.entrys[i];
                    allPaths = allPaths.concat(entry.file);
                }
                return excludePaths(allPaths);
            })()
        ),
        destinePath + '/css'
    ),
    js: mountPathSrcToDest('/js/**/*.js', '/js'),
    sass: mountPathSrcToDest('/sass/**/*.{sass,scss}', '/css'),
    pug: mountPathSrcToDest('/pug/**/*.pug', ''),
    html: mountPathSrcToDest('/html/**/*.{htm,html}', ''),
    docs: mountPathSrcToDest('/js/**/*.js', '/docs')
};

module.exports = { paths, sourcePath, destinePath };
