const { src, dest } = require('gulp');
const npmDist = require('gulp-npm-dist');
const rename = require('gulp-rename');

/** @const {GulpOptions} options */
const options = require('../configuration');

/**
 * Matches the configuration file to know what libs should import for the dist folder
 * TODO: Can be improved read from configuration file, package names to excluded.
 * @param {function} done
 */
function compile(done) {
    // Direct copy of the outsystems UI
    if (options.libs.outsystemsUI) {
        src('./src/libs/css/OutsystemsUIWeb/Theme.BaseTheme.css').pipe(
            dest('./dist/css/OutsystemsUIWeb/')
        );
    }
    if (options.libs.silkUI) {
        src('./src/libs/css/WebPatterns/Theme.Patterns_SilkUI.css').pipe(
            dest('./dist/css/WebPatterns/')
        );
    }

    src(npmDist(), { base: './node_modules' })
        .pipe(
            rename(function(path) {
                path.dirname = path.dirname
                    .replace(/\/dist/, '')
                    .replace(/\\dist/, '');
            })
        )
        .pipe(dest('./dist/libs'));

    done();
}

module.exports = { compile };
