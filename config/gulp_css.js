const { watch, src, series, dest } = require('gulp');
const postcss = require('gulp-postcss');
var concatCss = require('gulp-concat-css');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const rename = require('gulp-rename');
const cssvariables = require('postcss-css-variables');

const options = require('../configuration').css;
const paths = require('./gulp_path');

const sPath = paths.paths.css.sPath;
const dPath = paths.paths.css.dPath;

/**
 * Compiles files at src/css
 * @param {function} done
 */
function compile(done) {
    // get Files

    process(src(sPath));

    if (options.bundle.compile) {
        if (options.bundle.entrys.length) {
            for (let i = 0; i < options.bundle.entrys.length; i++) {
                const entry = options.bundle.entrys[i];
                process(src(entry.file).pipe(concatCss(entry.name)));
            }
        }
    }
    done();
}

/**
 * Reads the gulp options files and process the css files
 * with the options marked on the configuration file
 * @param {*} stream gulp stream
 * @return {*} gulp stream
 */
function process(stream) {
    if (options.cssvariables) {
        stream = stream.pipe(postcss([cssvariables()]));
    }
    if (options.autoprefixer) {
        stream = stream.pipe(postcss([autoprefixer({ grid: true })]));
    }
    stream.pipe(dest(dPath));
    if (options.minify) {
        stream
            .pipe(rename({ suffix: '.min' }))
            .pipe(postcss([cssnano()]))
            .pipe(dest(dPath));
    }
    return stream;
}

/**
 * Watch task for the css
 * @param {function} reload
 * @return {WatchMethod} Gulp Watch stream
 */
function csswatch(reload) {
    var fWCompileAndReload = series(compile, reload);

    return watch(paths.sourcePath + '/css/**/*.css')
        .on('change', fWCompileAndReload)
        .on('add', fWCompileAndReload);
}

module.exports = { compile, watch: csswatch };
