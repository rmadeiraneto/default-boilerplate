const { src, dest } = require('gulp');
var gulpDocumentation = require('gulp-documentation');

// Paths
const { sPath, dPath } = require('./gulp_path').paths.docs;
const packageprop = require('../package.json');

/**
 * Generates the docs for the js files in HTML pages
 * @param {function} done
 */
function generate(done) {
    src(sPath)
        .pipe(
            gulpDocumentation(
                'html',
                {},
                {
                    name: packageprop.name,
                    version: packageprop.version
                }
            )
        )
        .pipe(dest(dPath));
    done();
}

module.exports = { generate };
