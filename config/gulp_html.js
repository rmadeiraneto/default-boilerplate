const { watch, src, series, dest } = require('gulp');

const { sPath, dPath } = require('./gulp_path').paths.html;

/**
 * Clone the html files in the dist folder
 * @param {function} done
 */
function compile(done) {
    src(sPath).pipe(dest(dPath));
    done();
}

/**
 * Function that watch html files.
 * @param {function} reload
 * @return {WatchMethod} Gulp Watcher
 */
function htmlwatch(reload) {
    var fWCompileAndReload = series(compile, reload);

    return watch(sPath)
        .on('change', fWCompileAndReload)
        .on('add', fWCompileAndReload);
}

module.exports = { compile, watch: htmlwatch };
