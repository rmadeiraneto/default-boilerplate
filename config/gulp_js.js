const { watch, src, series, dest } = require('gulp');

const gulpBabel = require('gulp-babel');
const rollup = require('rollup');
const rollupResolve = require('rollup-plugin-node-resolve');
const rollupBabel = require('rollup-plugin-babel');

const { sPath, dPath } = require('./gulp_path').paths.js;

/**
 * compiles all js files to the dist folder and process them via babel
 * @param {function} cbDone
 */
function compile(cbDone) {
    src(sPath)
        .pipe(
            gulpBabel({
                presets: ['@babel/env']
            })
        )
        .pipe(dest(dPath));
    cbDone();
}
/**
 * TODO: not ready yet
 * Bundles js files into UMD for now
 * @param {function} done
 */
function bundle(done) {
    rollup
        .rollup({
            input: './src/js/main.umd.js',
            plugins: [
                rollupResolve(),
                rollupBabel({
                    exclude: 'node_modules/**' // only transpile our source code
                })
            ]
        })
        .then(bundle => {
            return bundle.write({
                file: './dist/library.js',
                format: 'umd',
                name: 'library',
                sourcemap: true
            });
        });
    done();
}

/**
 * Watch changes in the js files and compiles everything
 * @param {function} reload
 * @return {WatchMethod}
 */
function jswatch(reload) {
    var fWCompileAndReload = series(compile, reload);

    return watch(sPath)
        .on('change', fWCompileAndReload)
        .on('add', fWCompileAndReload);
}

module.exports = { compile, bundle, watch: jswatch };
