const { watch, src, series, dest } = require('gulp');
const pug = require('gulp-pug');
const { sPath, dPath } = require('./gulp_path').paths.pug;
/**
 * @param {function} done
 */
function compile(done) {
    src(sPath)
        .pipe(pug({ doctype: 'html', pretty: true }))
        .pipe(dest(dPath));
    done();
}

/**
 * @param {function} reload
 * @return {object}
 */
function pugWatch(reload) {
    var fWCompileAndReload = series(compile, reload);

    return watch(sPath)
        .on('change', fWCompileAndReload)
        .on('add', fWCompileAndReload);
}

module.exports = { compile, watch: pugWatch };
