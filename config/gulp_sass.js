const { watch, src, series, dest } = require('gulp');
const sass = require('gulp-sass');
const prettier = require('gulp-prettier');
const prettierconfig = require('../.prettierrc.js');
const { sPath, dPath } = require('./gulp_path').paths.sass;

/**
 * compile sass files into css
 * @param {function} done
 */
function compile(done) {
    src(sPath)
        .pipe(sass({ outputStyle: 'expanded' }))
        .pipe(prettier(prettierconfig))
        .pipe(dest(dPath));
    done();
}

/**
 * watch sass files changes and re-compile everything
 * @param {*} reload
 * @return {WatchMethod}
 */
function sassWatch(reload) {
    var fWCompileAndReload = series(compile, reload);

    return watch(sPath)
        .on('change', fWCompileAndReload)
        .on('add', fWCompileAndReload);
}

module.exports = { compile, watch: sassWatch };
